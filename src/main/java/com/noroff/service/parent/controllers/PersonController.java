package com.noroff.service.parent.controllers;

import com.noroff.service.parent.data_access.PersonRepository;
import com.noroff.service.parent.models.Pair;
import com.noroff.service.parent.models.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/person")
public class PersonController {

    PersonRepository personRepository = new PersonRepository();

    @GetMapping("/username/{username}")
    public ResponseEntity<Person> getUserByUsername(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Person person = personRepository.getPersonByUsername(username);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @PostMapping()
    public ResponseEntity<Person> postPerson(@RequestBody Person person){
        Pair<HttpStatus, Integer> res = personRepository.addPerson(person);

        if(res.getU() != -1) {
            person.setId(res.getU());
        }

        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(person, res.getT());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Person> updatePerson(@PathVariable int id, @RequestBody Person person){
        Pair<HttpStatus, Integer> status = personRepository.updatePerson(id, person);

        if(status.getU() != -1){
            person.setId(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(person, status.getT());
    }
}
