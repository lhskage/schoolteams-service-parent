package com.noroff.service.parent.controllers;

import com.noroff.service.parent.data_access.OptionalRepository;
import com.noroff.service.parent.models.Optional;
import com.noroff.service.parent.models.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/optional")
public class OptionalController {

    OptionalRepository optionalRepository = new OptionalRepository();

    @GetMapping("/user/{username}")
    public ResponseEntity<Optional> getSpecificOptionalInformation(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Optional optional = optionalRepository.getSpecificOptionalInformation(username);

        if(optional == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(optional, status);
    }

    @PostMapping()
    public ResponseEntity<Optional> addOptional(@RequestBody Optional optional){
        Pair<HttpStatus, Integer> res = optionalRepository.addOptional(optional);

        if(res.getU() != -1){
            optional.setId(res.getU());
        }
        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(optional, res.getT());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Optional> updatePerson(@PathVariable int id, @RequestBody Optional optional){
        Pair<HttpStatus, Integer> status = optionalRepository.updateOptional(id, optional);

        if(status.getU() != -1){
            optional.setId(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(optional, status.getT());
    }

    @PutMapping("/update/{username}")
    public ResponseEntity<Optional> updatePlayerOptional(@PathVariable String username, @RequestBody Optional optional){
        Pair<HttpStatus, Integer> status = optionalRepository.updatePlayerOptional(username, optional);

        if(status.getU() != -1){
            optional.setId(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(optional, status.getT());
    }
}
