package com.noroff.service.parent.controllers;

import com.noroff.service.parent.data_access.UserRepository;
import com.noroff.service.parent.models.Toggle2FA;
import com.noroff.service.parent.models.TwoFaQr;
import com.noroff.service.parent.models.User;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/user")
public class UserController {
    UserRepository userRepository = new UserRepository();

    @GetMapping()
    public ResponseEntity<ArrayList<User>> getAllUsers(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllUsers();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @PostMapping("/add")
    public ResponseEntity<User> addUser(@RequestBody User user){
        HttpStatus status = userRepository.addUser(user);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(user, status);
    }

    @PutMapping("/update/{username}")
    public ResponseEntity<User> updateUser(@PathVariable String username, @RequestBody User user) {
        HttpStatus status = userRepository.updateUser(username, user);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(user, status);
    }

    @GetMapping("/admin")
    public ResponseEntity<ArrayList<User>> getAllAdministrators(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllAdministrators();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @GetMapping("/coach")
    public ResponseEntity<ArrayList<User>> getAllCoaches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllCoaches();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @GetMapping("/parents")
    public ResponseEntity<ArrayList<User>> getAllParents(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllParents();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @GetMapping("/players")
    public ResponseEntity<ArrayList<User>> getAllPlayers(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllPlayers();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @GetMapping("/2fa/active/{username}")
    public ResponseEntity<Boolean> get2faActive(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Boolean active = userRepository.getActive(username);

        if(active == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(active, status);
    }

    @PatchMapping("/2fa")
    public ResponseEntity<Boolean> toggleActive2fa(@RequestBody Toggle2FA toggle2FA){
        HttpStatus status = HttpStatus.OK;
        Boolean active = userRepository.update2FA(toggle2FA);

        if(active == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        check2faData(toggle2FA.getUsername());

        return new ResponseEntity<>(active, status);
    }

    @GetMapping("/2fa/qr/{username}")
    public ResponseEntity<TwoFaQr> get2faQrUrl(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        String url = userRepository.getQrUrl(username);

        if(url == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new TwoFaQr(url), status);
    }

    private void check2faData(String username){
        TwoFaQr url = get2faQrUrl(username).getBody();
        if(url == null){
            //Sends request to verify the token
            String generateUrl = "https://schoolteams-service-auth.herokuapp.com/api/v1/2fa/generate";
            RestTemplate restTemplate = new RestTemplate();

            Map<String, String> requestBody = new HashMap();
            requestBody.put("username", username);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, headers);

            ResponseEntity<?> responseEntity = restTemplate.postForEntity(
                    generateUrl,
                    requestEntity,
                    String.class);
            HttpStatus statusCode = responseEntity.getStatusCode();
            //If the response code is 200, then the token is valid
            boolean authorized = statusCode == HttpStatus.OK;
        }
    }
}