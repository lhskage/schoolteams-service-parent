package com.noroff.service.parent.controllers;

import com.noroff.service.parent.data_access.ChildRepository;
import com.noroff.service.parent.models.Child;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/child")
public class ChildController {

    ChildRepository childRepository = new ChildRepository();

    @GetMapping("/parent/{username}")
    public ResponseEntity<ArrayList<Child>> getChildrenByParent(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Child> children = childRepository.getChildrenByParent(username);

        if(children == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(children, status);
    }

    @PostMapping("/addChildWithTwoParents")
    public ResponseEntity<Child> addChildWithTwoParents(@RequestBody Child child){
        HttpStatus status = childRepository.addChildWithTwoParents(child);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(child, status);
    }

    @PostMapping("/addChildWithOneParent")
    public ResponseEntity<Child> addChildWithOneParent(@RequestBody Child child){
        HttpStatus status = childRepository.addChildWithOneParent(child);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(child, status);
    }

    @PutMapping("/{username}")
    public ResponseEntity<Child> updateChildParentRelationship(@PathVariable String username, @RequestBody Child child) {
        HttpStatus status = childRepository.updateChildParentRelationship(username, child);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(child, status);
    }
}
