package com.noroff.service.parent.controllers;

import com.noroff.service.parent.data_access.LocationRepository;
import com.noroff.service.parent.models.Location;
import com.noroff.service.parent.models.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/location")
public class LocationController {

    LocationRepository locationRepository = new LocationRepository();

    @PostMapping("/new")
    public ResponseEntity<Location> addOptional(@RequestBody Location location){
        Pair<HttpStatus, Integer> res = locationRepository.addLocation(location);

        if(res.getU() != -1){
            location.setId(res.getU());
        }
        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(location, res.getT());
    }
}
