package com.noroff.service.parent.data_access;

import com.noroff.service.parent.models.Optional;
import com.noroff.service.parent.models.Pair;
import org.springframework.http.HttpStatus;

import java.sql.*;

public class OptionalRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public Optional getSpecificOptionalInformation(String username){
        Optional optionalInformation = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM optional " +
                    "JOIN person " +
                    "ON optional.id = person.optional_id " +
                    "JOIN users " +
                    "ON person.id = users.person_id " +
                    "WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){optionalInformation = new Optional(
                result.getInt("id"),
                result.getObject("date_of_birth", java.time.LocalDateTime.class),
                result.getString("mobile_number"),
                result.getString("profile_picture"),
                result.getString("medical_notes"),
                result.getBoolean("dob_shared"),
                result.getBoolean("mobile_number_shared"),
                result.getBoolean("profile_picture_shared"),
                result.getBoolean("medical_notes_shared")
            );}

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return optionalInformation;
    }

    public Pair<HttpStatus, Integer> addOptional(Optional optional){
        HttpStatus success;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO optional(date_of_birth, mobile_number, profile_picture, medical_notes) " +
                            "VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setObject(1, optional.getDate_of_birth());
            prep.setString(2, optional.getMobile_number());
            prep.setString(3, optional.getProfile_picture());
            prep.setString(4, optional.getMedical_notes());

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    public Pair<HttpStatus, Integer> updateOptional(int id, Optional optional) {
        HttpStatus success;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE optional " +
                            "SET (date_of_birth, mobile_number, profile_picture, medical_notes) = (?, ?, ?, ?) " +
                            "WHERE id = ?", Statement.RETURN_GENERATED_KEYS);

            prep.setObject(1, optional.getDate_of_birth());
            prep.setString(2, optional.getMobile_number());
            prep.setString(3, optional.getProfile_picture());
            prep.setString(4, optional.getMedical_notes());
            prep.setInt(5, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    public Pair<HttpStatus, Integer> updatePlayerOptional(String username, Optional optional) {
        HttpStatus success;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE optional " +
                            "SET (date_of_birth, mobile_number, profile_picture, medical_notes) = (?, ?, ?, ?)" +
                            "FROM person, users, player " +
                            "WHERE optional.id = person.optional_id " +
                            "AND person.id = users.person_id " +
                            "AND users.username = ?;", Statement.RETURN_GENERATED_KEYS);

            prep.setObject(1, optional.getDate_of_birth());
            prep.setString(2, optional.getMobile_number());
            prep.setString(3, optional.getProfile_picture());
            prep.setString(4, optional.getMedical_notes());
            prep.setString(5, username);


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }
}

