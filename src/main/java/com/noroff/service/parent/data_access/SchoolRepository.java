package com.noroff.service.parent.data_access;

import com.noroff.service.parent.models.School;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SchoolRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    public School getSchoolByPlayer(String username) {
        School school = new School();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement(
                    "SELECT school.id, school.name FROM school " +
                            "JOIN team " +
                            "ON school.id = team.school_id " +
                            "JOIN player " +
                            "ON team.id = player.team_id " +
                            "WHERE player.user_username = ?");
            prep.setString(1, username);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                school = new School(
                    result.getInt("id"),
                    result.getString("name")
                );
            }
        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return school;
    }
}
