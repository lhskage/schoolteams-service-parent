package com.noroff.service.parent.data_access;

import com.noroff.service.parent.models.Location;
import com.noroff.service.parent.models.Pair;
import org.springframework.http.HttpStatus;

import java.sql.*;

public class LocationRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;



    public Pair<HttpStatus, Integer> addLocation(Location location) {
        HttpStatus success;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO location(street, number, postal_code, name) " +
                            "VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, location.getStreet());
            prep.setString(2, location.getNumber());
            prep.setInt(3, location.getPostal_code());
            prep.setString(4, location.getName());

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }
}