package com.noroff.service.parent.data_access;

import com.noroff.service.parent.models.Team;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TeamRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Team> getAllTeams() {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team;");
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                        new Team(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getInt("school_id"),
                                result.getString("coach"),
                                result.getInt("sport_id")
                        )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public Team getTeamById(int team_id) {
        Team team = null;

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team " +
                    "WHERE id = ?;");
            prep.setInt(1, team_id);
            ResultSet result = prep.executeQuery();

            while(result.next()){team = new Team(
                result.getInt("id"),
                result.getString("name"),
                result.getInt("school_id"),
                result.getString("coach"),
                result.getInt("sport_id")
            );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return team;
    }

    public ArrayList<Team> getTeamsBySchool(int school_id) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team " +
                    "WHERE school_id = ?;");
            prep.setInt(1, school_id);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                    new Team(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("school_id"),
                        result.getString("coach"),
                        result.getInt("sport_id")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public ArrayList<Team> getTeamByCoach(String coach) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team " +
                    "WHERE coach = ?;");
            prep.setString(1, coach);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                        new Team(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getInt("school_id"),
                                result.getString("coach"),
                                result.getInt("sport_id")
                        ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public ArrayList<Team> getTeamBySport(int sport_id) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team " +
                    "WHERE sport_id = ?;");
            prep.setInt(1, sport_id);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                    new Team(
                            result.getInt("id"),
                            result.getString("name"),
                            result.getInt("school_id"),
                            result.getString("coach"),
                            result.getInt("sport_id")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public ArrayList<Team> getTeamByPlayer(String player) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT team.id, team.name, team.school_id, team.coach, team.sport_id " +
                    "FROM team " +
                    "JOIN player " +
                    "ON team.id = player.team_id " +
                    "WHERE player.user_username = ?;");
            prep.setString(1, player);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                        new Team(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getInt("school_id"),
                                result.getString("coach"),
                                result.getInt("sport_id")
                        ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }
}
